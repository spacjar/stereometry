import React, { useRef, useState } from "react";
import { ReactThreeFiber, useFrame } from "@react-three/fiber";
import type {Mesh} from "three";

export interface BoxProps extends ReactThreeFiber.MeshProps {
    x?: number;
    y?: number;
    z?: number;
    isRotating?: boolean;
    primaryColor?: ReactThreeFiber.Color;
    secondaryColor?: string;
}

const Box: React.FC<BoxProps> = (props = {x:1, y:1, z:1, isRotating: true}) => {

    const { x, y, z, isRotating, primaryColor, secondaryColor, ...rest } = props;
    const mesh = useRef<Mesh>();

    const [hovered, setHovered] = useState(false);
    const [active, setActive] = useState(false);

    useFrame(() => {
        if (props.isRotating)
            if (mesh.current) {
                mesh.current!.rotation.x += 0.01;
                mesh.current!.rotation.y += 0.01;
            }
    })

return (
    <mesh
        {...rest}
        scale={active ? 1.5 : 1}
        onClick={(e) => setActive(!active)}
        onPointerOver={(e) => setHovered(true)}
        onPointerOut={(e) => setHovered(false)}
        ref={mesh}>
        <boxBufferGeometry args={[x, y, z]} />
        <meshStandardMaterial color={!hovered ? props.primaryColor : props.secondaryColor} />
    </mesh>
)};

export default Box;