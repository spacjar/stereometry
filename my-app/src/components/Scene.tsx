import { Canvas } from "@react-three/fiber";
import React, { useState } from "react";
import THREE from "three";
import Box, { BoxProps } from "./Box";
import Controls from "./Controls";

interface SceneProps extends BoxProps {
    isBox?: boolean;
}

const Scene: React.FC<SceneProps> = (props = {x: 1, y: 1, z: 1}) => {

    const [isRotating, setIsRotating] = useState(false);

return (
    <div className="canvasContainerFlex">
        <div className="canvasContainer">
            <Canvas>
                <ambientLight />
                <pointLight position={[10, 10, 10]} />
                <Box
                    x={props.x}
                    y={props.y}
                    z={props.z}
                    isRotating={isRotating}
                    position={[0,0,0]}
                    primaryColor={props.primaryColor}
                    secondaryColor={props.secondaryColor} />
                <Controls />
            </Canvas>
        </div>
        <div className="canvasCard">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Esse similique error ipsa incidunt sunt, adipisci officiis? Incidunt exercitationem consectetur temporibus praesentium nostrum sint at voluptatem harum, officia, consequuntur ex. Perferendis numquam vero, fugit totam a dolorum quis. Sunt, tenetur enim!</p>
            <button onClick={(e) => setIsRotating(!isRotating)}>{!isRotating ? "Rotate" : "Stop Rotating"}</button>
        </div>
    </div>
)};

export default Scene;