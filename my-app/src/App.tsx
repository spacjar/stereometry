import React from 'react';
import './App.css';
import Scene from './components/Scene';

const App: React.FC = () => (
  <>
    <Scene primaryColor={"hotpink"} secondaryColor={"purple"} />
    <hr/>
    <Scene primaryColor={"hotpink"} secondaryColor={"purple"} x={3} y={2} />
  </>
)

export default App;
